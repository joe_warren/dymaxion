package it.doscienceto.mapping.dymaxion

object Dymax {
  type Vec2 = (Double, Double);
  type Vec3 = (Double, Double, Double);
  
  private def toXYZ(lat:Double,long:Double):Vec3={
    val x = Math.sin(lat)*Math.cos(long);
    val y = Math.sin(lat) * Math.sin(long);
    val z = Math.cos(lat);
    (x,y,z)
  }
  private def toSpherical(point:Vec3): Vec2={
    val (x,y,z) = point;
    val a =  if (x>0.0 && y>0.0){0.0}
    else if (x<0.0 && y>0.0){Math.PI}
    else if (x<0.0 && y<0.0){Math.PI}
    else {2.0*Math.PI};
    
    val lat = Math.acos(z);
    
    val long = if (x==0.0 && y>0.0){Math.PI/2.0}
    else if (x==0.0 && y<0.0){3.0*Math.PI/2.0}
    else if (x>0.0 && y==0.0){0.0}
    else if (x<0.0 && y==0.0){Math.PI}
    else {Math.atan(y/x) + a};
    (lat, long)
  }
  private def rotateX(p:Vec3, a:Double):Vec3 ={
    val c = Math.cos(a);
    val s = Math.sin(a);
    (p._1, p._2*c + p._3*s, p._3*c - p._2*s)
  }
  private def rotateY(p:Vec3, a:Double):Vec3 ={
    val c = Math.cos(a);
    val s = Math.sin(a);
    (p._1*c - p._3*s, p._2, p._3*c + p._1*s)
  }
  private def rotateZ(p:Vec3, a:Double):Vec3 ={
    val c = Math.cos(a);
    val s = Math.sin(a);
    (p._1*c + p._2*s, p._2*c - p._1*s, p._3)
  }
  private def rotate( p:Vec2, a:Double):Vec2 ={
    val c = Math.cos(a);
    val s = Math.sin(a);
    (p._1*c - p._2*s, p._2*c + p._1*s)
  }
  
   val vertices = List(
     (0.0, 0.0, 0.0), 
     (0.420152426708710003, 0.078145249402782959, 0.904082550615019298),
     (0.995009439436241649, -0.091347795276427931, 0.040147175877166645),
     (0.518836730327364437, 0.835420380378235850, 0.181331837557262454),
     (-0.414682225320335218, 0.655962405434800777, 0.630675807891475371),
     (-0.515455959944041808, -0.381716898287133011, 0.767200992517747538),
     (0.355781402532944713, -0.843580002466178147, 0.402234226602925571),
     (0.414682225320335218, -0.655962405434800777, -0.630675807891475371),
     (0.515455959944041808, 0.381716898287133011, -0.767200992517747538),
     (-0.355781402532944713, 0.843580002466178147, -0.402234226602925571),
     (-0.995009439436241649, 0.091347795276427931, -0.040147175877166645),
     (-0.518836730327364437, -0.835420380378235850, -0.181331837557262454),
     (-0.420152426708710003, -0.078145249402782959, -0.904082550615019298)
   );
   val faces = List(
       (0, 0, 0),
       (1, 2, 3),
       (1, 3, 4),
       (1, 5, 4),
       (1, 6, 5),
       (1, 2, 6),
       (2, 3, 8),
       (3, 9, 8),
       (3, 4, 9), 
       (4, 10, 9),
       (4, 5, 10),
       (5, 11, 10),
       (5, 6, 11),
       (6, 7, 11),
       (2, 7, 6),
       (2, 8, 7),
       (8, 9, 12),
       (9, 10, 12),
       (10, 11, 12),
       (11, 7, 12),
       (8, 12, 7)
   );
   val garc = 2.0 * Math.asin( Math.sqrt( 5.0 - Math.sqrt(5.0)) / Math.sqrt(10.0) );
   val gt = garc / 2.0;

   val gdve = Math.sqrt( 3.0 + Math.sqrt(5) ) / Math.sqrt( 5.0 + Math.sqrt(5.0) );
   val gel = Math.sqrt(8.0) / Math.sqrt(5.0 + Math.sqrt(5.0));
   
   private def magnitude( v:Vec3): Double = {
     Math.sqrt( v._1*v._1 + v._2*v._2 + v._3*v._3 )
   }
   
   private def distance(a:Vec3, b:Vec3):Double={
     magnitude( (a._1-b._1, a._2-b._2, a._3-b._3) ) 
   }
   
   def calcCenter(a:Vec3, b:Vec3, c:Vec3 ):Vec3={
     val t = ((a._1 + b._1 + c._1)/3.0, 
              (a._2 + b._2 + c._2)/3.0,
              (a._3 + b._3 + c._3)/3.0
              );
     val magn = magnitude(t);
     (t._1/magn, t._2/magn, t._3/magn)
   }
   
   val centers = faces
     .map({ case(a,b,c)=> (vertices(a), vertices(b), vertices(c)) })
     .map({ case (a,b,c) => calcCenter(a, b, c) });    
  
   private def triangleIndices(point:Vec3):(Int, Int)={
     val dists = centers.map(distance(point, _));
     val faceInd = dists.zipWithIndex.min._2;
     val vInds = faces(faceInd);
     val dist1 = distance(point, vertices(vInds._1))
     val dist2 = distance(point, vertices(vInds._2))
     val dist3 = distance(point, vertices(vInds._3))
     
     val lcd = if ( (dist1 <= dist2) && (dist2 <= dist3) ) {1}
       else if ( (dist1 <= dist3) && (dist3 <= dist2) ) {6}
       else if ( (dist2 <= dist1) && (dist1 <= dist3) ) {2}
       else if ( (dist2 <= dist3) && (dist3 <= dist1) ) {3}
       else if ( (dist3 <= dist1) && (dist1 <= dist2) ) {5}
       else if ( (dist3 <= dist2) && (dist2 <= dist1) ) {4}
       else {0};
     (faceInd, lcd)
   }
   private def dymaxionPoint( faceIndex: Int, lcdIndex:Int, point:Vec3):Vec2 ={
     val v1 = faces(faceIndex)._1;
    
     val h1 = vertices(v1);
     val center = centers(faceIndex);
     val (clat, clong) = toSpherical(center);
     
     val h0r1 = rotateY( rotateZ(point, clong), clat);
     val h1r1 = rotateY( rotateZ(h1, clong), clat);
     
     val (h1r1lat, h1r1long) = toSpherical(h1r1);
     val h0r2 = rotateZ(h0r1, h1r1long - Math.PI/2.0);
     
     val gz = Math.sqrt( 1.0 - h0r2._1*h0r2._1 - h0r2._2*h0r2._2 );
     val gs = Math.sqrt( 5.0 + 2.0 * Math.sqrt(5.0) )/ (gz*Math.sqrt(15.0) );
     
     val gxp = h0r2._1 * gs;
     val gyp = h0r2._2 * gs;
     
     
     val ga1p = 2.0 * gyp / Math.sqrt(3.0) + (gel / 3.0) ;
     val ga2p = gxp - (gyp / Math.sqrt(3)) +  (gel / 3.0) ;
     val ga3p = (gel / 3.0) - gxp - (gyp / Math.sqrt(3));

     val ga1 = gt + Math.atan( (ga1p - 0.5 * gel) / gdve);
     val ga2 = gt + Math.atan( (ga2p - 0.5 * gel) / gdve);
     val ga3 = gt + Math.atan( (ga3p - 0.5 * gel) / gdve);
     
     val gx = 0.5 * (ga2 - ga3) ;
     val gy = (1.0 / (2.0 * Math.sqrt(3.0)) ) * (2.0 * ga1 - ga2 - ga3);

     val p = (gx / garc, gy / garc);
     
    faceIndex match {
      case 1 => { 
        val (x, y) = rotate(p, 4.0*Math.PI/3.0);
	    (x + 2.0, y + 7.0 / (2.0 * Math.sqrt(3.0)))
       }
       case 2 => {
         val (x, y) = rotate(p, 300.0*Math.PI/180.0); 
         (x + 2.0, y + 5.0 / (2.0 * Math.sqrt(3.0)))
       }
       case 3 => {
         val (x, y) = rotate(p, 0.0);
         (x + 2.5, y + 2.0 / Math.sqrt(3.0))
       }
       case 4 => { 
         val (x, y) = rotate(p, Math.PI/3.0);
         (x + 3.0, y + 5.0 / (2.0 * Math.sqrt(3.0)))
       }
       case 5 => {
         val(x, y) = rotate(p, Math.PI);
	     (x + 2.5, y + 4.0 * Math.sqrt(3.0) / 3.0)
       }
       case 6 => {
         val(x, y) = rotate(p, 300.0 * Math.PI/180.0);
         (x + 1.5, y + 4.0 * Math.sqrt(3.0) / 3.0)
       }
       case 7 =>{
         val (x, y) = rotate(p, 300.0*Math.PI/180.0);
         (x + 1.0, y + 5.0 / (2.0 * Math.sqrt(3.0)))
       }
       case 8 => {
         val (x,y) = rotate(p, 0.0);
         (x + 1.5, y + 2.0 / Math.sqrt(3.0))
       }
       case 9 => {
         if (lcdIndex > 2){
	       val (x, y) = rotate(p, 300.0*Math.PI/180.0);
	       (x + 1.5, y + 1.0 / Math.sqrt(3.0))
	     } else {
	      val (x, y) = rotate(p, 0.0);
	      (x + 2.0, y + 1.0 / (2.0 * Math.sqrt(3.0)))
	     }
       }
       case 10 => {
         val (x, y) = rotate(p, Math.PI/3.0);
         (x + 2.5, y + 1.0 / Math.sqrt(3.0));
       }
       case 11 => {
         val (x, y) = rotate(p, Math.PI/3.0);
         (x + 3.5, y + 1.0 / Math.sqrt(3.0))
       }
       case 12 => {
         val(x,y) = rotate(p, 2.0*Math.PI/3.0);
         (x + 3.5, y + 2.0 / Math.sqrt(3.0))
       }
       case 13 => {
         val (x, y) = rotate(p, Math.PI/3.0);
         (x + 4.0, y + 5.0 / (2.0 * Math.sqrt(3.0)));
       }
       case 14 => {
         val (x,y) = rotate(p, 0.0);
	     (x + 4.0, y + 7.0 / (2.0 * Math.sqrt(3.0)))
       }
       case 15 => {
         val (x, y) = rotate(p, 0.0);
	     (x + 5.0, y + 7.0 / (2.0 * Math.sqrt(3.0)))
       }
       case 16=> {
         if (lcdIndex < 4){
		   val (x,y) = rotate(p, Math.PI/3.0);
		   (x + 0.5, y + 1.0 / Math.sqrt(3.0))
	     } else {
		   val (x, y) = rotate(p, 0.0);
		   (x + 5.5, y + 2.0 / Math.sqrt(3.0));
	     }
       }
       case 17 => {
         val (x,y) = rotate(p, 0.0);
       
	     (x + 1.0, y + 1.0 / (2.0 * Math.sqrt(3.0)))
       }
       case 18 => {
         val (x,y) = rotate(p, 2.0*Math.PI/3.0);
         (x + 4.0, y + 1.0 / (2.0 * Math.sqrt(3.0)))
       }
       case 19 => {
         val (x, y) = rotate(p, 2.0*Math.PI/3.0);
         (x + 4.5, y + 2.0 / Math.sqrt(3.0));
       }
       case default => { // 20
         val (x, y) = rotate(p, 300.0*Math.PI/180);
         (x + 5.0, y + 5.0 / (2.0 * Math.sqrt(3.0)))
       }
     }
   }
  def toDymaxion(lat:Double, long:Double):(Double,Double)={
    val point = toXYZ(lat, long);
    val (faceIndex, lcdIndex) = triangleIndices(point); 
    dymaxionPoint(faceIndex, lcdIndex, point)
  }
}