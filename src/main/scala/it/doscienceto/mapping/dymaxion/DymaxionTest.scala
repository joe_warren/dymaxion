package it.doscienceto.mapping.dymaxion
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

object DymaxionTest {
  def main(args:Array[String]):Unit={
     val input = ImageIO.read(new File(args(0)));
     val output = new BufferedImage(4000, 2000, BufferedImage.TYPE_INT_RGB);
     val width = input.getWidth();
     val height = input.getHeight();
     for(i <- Range(0, width) ){
       for( j<- Range(0, height) ){
         val col = input.getRGB(i, j); 
         val(x, y) = Dymax.toDymaxion(j*Math.PI/height, Math.PI+2.0*i*Math.PI/width);
         output.setRGB((x*600).toInt, 1800-(y*600).toInt, col);
       }
     }
     ImageIO.write(output, "JPG", new File(args(1)))
  }
}